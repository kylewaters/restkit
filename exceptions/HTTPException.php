<?php
namespace Momentum\Exceptions;

class HTTPException extends \Exception
{

	public $devMessage;
	public $errorCode;
	public $response;
	public $additionalInfo;

	public function __construct($message, $code, $errorArray)
    {
		$this->message = $message;
		$this->devMessage = @$errorArray['dev'];
		$this->userMessage = @$errorArray['user'];
		$this->errorCode = @$errorArray['internalCode'];
		$this->code = $code;
		$this->additionalInfo = @$errorArray['more'];
		$this->response = $this->getResponseDescription($code);
	}

	public function send()
    {
		$di = \Phalcon\DI::getDefault();

		$res = $di->get('response');
		$req = $di->get('request');

		if(!$req->get('suppress_response_codes', null, null)) {

            if($this->response == 'Unknown Status Code') {

                $http_status_code = \Momentum\Responses\Http::INTERNAL_SERVER_ERROR;
                $res->setStatusCode($http_status_code, 'Internal Error')->sendHeaders();

            }else{

                $http_status_code = $this->getCode();
			    $res->setStatusCode($this->getCode(), $this->response)->sendHeaders();

            }

		}else{

			$res->setStatusCode('200', 'OK')->sendHeaders();

		}

        $exception_data = array(
            'status'       => $http_status_code,
            'href'         => $this->getUri(),
            'code'         => $this->getCode(),
            'internalCode' => $this->errorCode,
            'error'        => $this->getMessage(),
            'userMessage'  => $this->userMessage,
            'devMessage'   => $this->devMessage,
            'more'         => $this->additionalInfo,
        );



		if(!$req->get('type') || $req->get('type') == 'json') {

            $scheme = new \Momentum\Responses\Schemas\Exception($exception_data);
            $response = new \Momentum\Responses\JSONResponse();
			$response->send($scheme);

		} else if($req->get('type') == 'csv') {

			$response = new \Momentum\Responses\CSVResponse();
			$response->send(array($exception_data));

		}
		
		error_log('HTTPException: ' . $this->getFile() . ' at ' . $this->getLine());

		return true;
	}

	protected function getResponseDescription($code)
	{
		$codes = array(

			// Informational 1xx
			100 => 'Continue',
			101 => 'Switching Protocols',

			// Success 2xx
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',

			// Redirection 3xx
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',  // 1.1
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',

			// 306 is deprecated but reserved
			307 => 'Temporary Redirect',

			// Client Error 4xx
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',

			// Server Error 5xx
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			509 => 'Bandwidth Limit Exceeded'
		);

		$result = (isset($codes[$code])) ? $codes[$code] : 'Unknown Status Code';

		return $result;
	}

    /**
     * Gets Current URL for current resource / list navigation
     * TODO: Handel single item cases
     *
     * @return bool|string
     */
    protected function getUri()
    {
        return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    /**
     * Gets base URL for the request {http(s)}://{domain}/
     *
     * @return bool|string
     */
    public function baseUrl()
    {
        return sprintf(
            "%s://%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['HTTP_HOST']
        );
    }
}