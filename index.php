<?php

use Phalcon\Mvc\Micro\Collection,
	Phalcon\Config\Adapter\Ini as IniConfig;

require_once 'bootstrap.php';

try {

    $app = new Phalcon\Mvc\Micro($di);

    /**
     * Mount all of the collections, to makes the routes active.
     */
    foreach($di->get('collections') as $collection) { $app->mount($collection); }

    /**
     * After a route is run, usually when its Controller returns a final value,
     * the application runs the following function which actually sends the response to the client.
     *
     * The default behavior is to send the Controller's returned value to the client as JSON.
     * However, by parsing the request querystring's 'type' paramter, it is easy to install
     * different response type handlers.  Below is an alternate csv handler.
     */
    $app->after(function() use ($app) {

        // OPTIONS have no body, send the headers, exit
        if($app->request->getMethod() == 'OPTIONS') {

            $app->response->setStatusCode('200', 'OK');
            $app->response->send();
            return;

        }

        $accept = explode(',', $app->request->getHeader('ACCEPT'));

        switch($accept[0]) {

            default :
            case 'text/html' :
            case 'text/javascript' :
            case 'application/json' :
            case 'application/javascript' :

                $response_type = 'json';
                break;

            case 'application/xml' :
            case 'text/xml' :
                $response_type = 'xml';
                break;

            case 'text/comma-separated-values' :
            case 'application/excel' :
            case 'application/csv' :
            case 'text/csv' :
                $response_type = 'csv';
                break;

        }

        // Respond by default as JSON
        if(!$app->request->get('format') || $app->request->get('format') == 'json') {

            // Results returned from the route's controller.  All Controllers should return an array
            $output = $app->getReturnedValue();

            $response = new \Momentum\Responses\JSONResponse();
            $response->send($output['response'], $output['status']);

            return;

        }elseif($app->request->get('format') == 'csv') {

            $records = $app->getReturnedValue();
            $response = new \Momentum\Responses\CSVResponse();
            $response->useHeaderRow(true)->send($records);

            return;
        }

    });

    $app->handle();

}catch(\Exception $exception){

    //HTTPException's send method provides the correct response headers and body
    if(!is_a($exception, 'Momentum\\Exceptions\\HTTPException')){

        $errorArray['more'] = '[Support URL Here] Support Email: kyle@theorganicagency.com';
        $errorArray['user'] = 'Oops! A problem has occurred and I can\'t complete your request. Please try again or contact support';

        $exception_type = $exception instanceof PDOException ? 'Database' : 'Framework';

        $errorArray['dev'] = "$exception_type level exception occurred in {$exception->getFile()} on line {$exception->getLine()}.";

        //    $errorArray['dev'] .= ' Original exception '. get_class($exception). ' was converted to our own REST HTTPException then thrown from '. __FILE__ .':'. __LINE__;

        $exception = new Momentum\Exceptions\HTTPException($exception->getMessage(), $exception->getCode(), $errorArray);

    }

    $exception->send();

    error_log($exception);
    error_log($exception->getTraceAsString());

}
