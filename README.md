RestKit 0.1
============


By Kyle Waters kyle@theorganicagency.com


Recent changes
------------------------

 - Added state to JSON Response
 - Added sorting and direction to options
 - Added 'type' to meta data
 - Refactored results passing from Rest controller to response. See RestController->provide().
 - Added properties example
 - Changed 'data' folder to 'schema'
 - Removed unused files / folders
 - Removed phalcon-rest example data and files
 - Change search parameter name to ?search
 - Updated partial fields and allowed fields logic
 - Updated and cleaned up parse params code
 - Added Schemas
 - Changed JSON response schema and implemented HATEOAS best practises
 - Added search and field params to the response
 - Updated status in response to HTTP code (was "SUCCESS")
 - Added RESTDB controller for processing standard look ups via database
 - Change queries to use query builder instead of ORM for speed (200% faster look-ups)
 - Added APC caching for record look-ups
 - Removed version number from URL and added to content type (application/json;application&v=1)
 - Changed ETag to only generate for look ups under 20 records (was causing PHP memory limit errors for large result sets)
 - Updated exception handling - exceptions now aways return useful JSON response with user and developer messages

 ** The changes are now becoming too many to list, many parts have stayed a long way from the original framework now.


https://bitbucket.org/organicdevelopment/RestKit

Orginally based on a project for APIs using the [Phalcon][phalcon] framework based on PhalconRest by Sean Moore
-------------------------------------------------------------------------------------------------

Original Project - https://github.com/cmoore4/phalcon-rest

Many changed made based on research and recommendations for best practise REST API implementation.

http://www.youtube.com/watch?v=hdSrT4yjS1g
http://www.youtube.com/watch?v=ITmcAGvfcJI
http://www.youtube.com/watch?v=HW9wWZHWhnI

http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api


TODO List
---------

 - Implement getTotal() method and 'last' link
 - Complete implementation of single resource URLS
 - Uniform error and exception handling and corresponding output - NOW IN TESTING
 - Address error messages format, user message, developer message, codes etc.. - NOW IN TESTING
 - Add fallback system for custom Schema definitions
 - Added config options to collections
 - Address Accepts headers and response types workflow (if accepts CSV/plain-text) return in that order
 - Implement OAuth10.a or API keys
 - Implement expand parameter
 - Implement logging server via MongoDb
 - Implement socket access adapter for real-time
 - Complete the describe controller and functions
 - Implement StormPath for user management and auth


API Assumptions
---------------

**URL Structure**

```
base.tld/collection?search=(search1:value1,search2:value2)&columns=(field1,field2,field3)&limit=10&offest=20&type=csv
base.tld/resource/id?column=(field1,field2,field3)
```

**Request Bodies**

Request bodies will be submitted as valid JSON.

The Fields
-----------

**Search**

Searches are determined by the 'search' parameter.  Following that is a parenthesis enclosed list of key:value pairs, separated by commas.

> ex: search=(city:Exeter,title:Property 1)

**Search Type **

Changed search type. Filter uses 'AND' glue, default uses 'OR' glue.

> ex: search_type=filter

**Partial Responses**

Partial responses are used to only return certain explicit columns from a record. They are determined by the 'columns' parameter, which is a list of field names separated by commas, enclosed in parenthesis.

> ex: columns=(id,title,location)

**Limit and Offset**

Often used to paginate large result sets.  Offset is the record to start from, and limit is the number of records to return.

> ex: limit=20&offset=20   will return results 21 to 40

**Return Format**

Overrides any accept headers.  JSON is assumed otherwise.  Return type handler must be implemented.

> ex: format=xml

**Suppressed Error Codes**

Some clients require all responses to be a 200 (Flash, for example), even if there was an application error.
With this parameter included, the application will always return a 200 response code, and clients will be
responsible for checking the response body to ensure a valid response.

> ex: suppress_error_codes=true

Responses
---------

All route controllers must return an array.  This array is used to create the response object.

**JSON**

JSON is the default response type.  It comes with an envelope wrapper, so responses will look like this:

```
GET properties?search=(city:exeter)&offset=1&limit=2&columns=id,title

{
    "status": 200,
    "href": "http://organic-rest.loc/properties?limit=2&offset=1&sort=title&direction=desc&columns=id,title&search=(city:exeter)",
    "first": "http://organic-rest.loc/properties?limit=2&offset=0&sort=title&direction=desc&columns=id,title&search=(city:exeter)",
    "previous": "http://organic-rest.loc/properties?limit=2&offset=0&sort=title&direction=desc&columns=id,title&search=(city:exeter)",
    "next": "http://organic-rest.loc/properties?limit=2&offset=2&sort=title&direction=desc&columns=id,title&search=(city:exeter)",
    "count": 2,
    "limit": "2",
    "offset": "1",
    "sort": "title",
    "direction": "desc",
    "search": {
        "city": "exeter"
    },
    "columns": [
        "id",
        "title"
    ],
    "records": [
        {
            "id": "9005",
            "title": "Property 2"
        },
        {
            "id": "8005",
            "title": "Property 2"
        }
    ]
}

The envelope can be suppressed for responses via the 'envelope=false' query parameter.  This will return just the record set by itself as the body, and the meta information via X- headers.

Often times, database field names are snake_cased.  However, when working with an API, developers 
generally prefer JSON columns to be returned in camelCase (many API requests are from browsers, in JS).
This project will by default convert all keys in a records response from snake_case to camelCase.

This can be turned off for your API by setting the JSONResponse's function "convertSnakeCase(false)".

**CSV**

CSV is the other implemented handler.  It uses the first record's keys as the header row, and then creates a csv from each row in the array.  The header row can be toggled off for responses.

```
title,type,city
Property 1, Flat, Exeter
```

Errors
-------

Momentum\Exception\HTTPException extends PHP's native exceptions.  Throwing this type of exception
returns a nicely formatted JSON response to the client.

```
throw new \Momentum\Exceptions\HTTPException(
	'Could not return results in specified format',
	403,
	array(
		'dev' => 'Could not understand type specified by type paramter in query string.',
		'internalCode' => 'NF1000',
		'more' => 'Type may not be implemented. Choose either "csv" or "json"'	
	)
);
```

Returns this:

```
{
    "_meta": {
        "status": "ERROR"
    },
    "error": {
        "devMessage": "Could not understand type specified by type paramter in query string.",
        "error": 403,
        "errorCode": "NF1000",
        "more": "Type may not be implemented. Choose either \"csv\" or \"json\"",
        "userMessage": "Could not return results in specified format"
    }
}
```

[phalcon]: http://phalconphp.com/index
[phalconDocs]: http://docs.phalconphp.com/en/latest/
[apigeeBook]: https://blog.apigee.com/detail/announcement_new_ebook_on_web_api_design