<?php
namespace Momentum\Responses;

class JSONResponse extends Response
{
	public function send(\Momentum\Responses\Schemas\Schema $schema, $status)
    {
        // Error's come from HTTPException.  This helps set the proper envelope data
		$response = $this->di->get('response');

		$request = $this->di->get('request');

        // Store a record count before the schema gets array converted
        $count = $schema->getCount();

        // If the envelope is on (default), the JSON response will contain
        // meta, state and links data. If envelope is false, the response
        // will contain records only.
		if($request->get('envelope', 'string', 'true') !== 'false') $body = $schema->full();
		else $body = isset($schema->records) ?: array();

        // Set response headers
		$response->setContentType('application/json,vnd.restkit.v1+json'); // TODO: Application name should come from config
        $response->setHeader('X-Powered-By', 'Digital Momentum RestKit');  // TODO: Application name should come from config
        $response->setHeader('X-Record-Count', isset($schema->records) ? $schema->records : 0);
        $response->setHeader('X-RestKit-Media-Type', 'RestKit.v1');        // TODO: Application name should come from config
        if($count > 0 && $count <= 20) $response->setHeader('E-Tag', md5(serialize($body['items'])));

		// HEAD requests are detected in the parent constructor. HEAD does everything exactly the
		// same as GET, but contains no body.
		if(!$this->head) $response->setJsonContent($body);

		$response->send();

		return $this;
	}
}