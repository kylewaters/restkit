<?php

namespace Momentum\Responses\Schemas;

class Exception extends Schema {

    public $status = '';

    public $href = '';

    public $code = '';

    public $internalCode = '';

    public $error = '';

    public $userMessage = '';

    public $devMessage = '';

    public $more = '';

    public function full()
    {
        $result = (array) $this;

        return $result;
    }

}