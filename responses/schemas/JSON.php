<?php
namespace Momentum\Responses\Schemas;

class Json extends Schema {

    public $status = '';

    public $href = '';

    public $first = '';

    public $previous = '';

    public $next = '';

  //  public $last = '';

    public $count = 0;

    public $limit = 0;

    public $offset = 0;

    public $sort = '';

    public $direction = '';

    public $search = '';

    public $columns = '';

    public $items = array();

    public function __construct($data) {

        // If you need to manipulate the incoming data to
        // build your own schema you can do so in here.

        parent::__construct($data);

    }

    public function full()
    {
        $result = (array) $this;

        return $result;
    }

    public function records()
    {
        $result = array('records' => (array) $this->records);

        return $result;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getCount()
    {
        return $this->count;
    }

}