<?php

namespace Momentum\Controllers;
use \Momentum\Exceptions\HTTPException;


class CompaniesController extends RESTDBController {

    /**
     * Sets which fields may be searched against, and which fields are allowed to be returned in
     * partial responses.  This will be overridden in child Controllers that support searching
     * and partial responses.
     * @var array
     */
    protected $allowedFields = array(
        'search'   => array('postcode', 'website', 'e-mail'),
        'columns' => array('*')
    );

    /**
     * Sets the default sort column
     *
     * @var string
     */
    // protected $sort = 'title';

    /**
     * Sets the default sort direction
     *
     * @var string
     */
    protected $direction = 'asc';


}