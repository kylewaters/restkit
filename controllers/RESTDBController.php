<?php
namespace Momentum\Controllers;
use \Momentum\Exceptions\HTTPException;
use Phalcon\Mvc\Model\Resultset;

class RESTDBController extends RESTController {

    /**
     * TODO: Implement fast get total method
     *
     * @param array $context
     * @return mixed
     */
    public function getTotal($context = array())
    {
        $router = new \Phalcon\Mvc\Router();
        $router->handle();
        $name =  ucfirst($router->getControllerName());

        $context = array_merge(array('models' => array($name),), $context);

        $queryBuilder = new \Phalcon\Mvc\Model\Query\Builder($context);

        // Build search conditions
        if($this->isSearch) {

            $context['conditions'] = '';
            $context['bind'] = array();

            $search_glue = $this->searchType == 'filter' ? 'AND' : 'OR';

            $last_field = @end(array_keys($this->searchFields));
            foreach ($this->searchFields as $field => $value) {

                $context['conditions'] .= " $field LIKE :{$field}_value:";
                $context['bind'][$field.'_value'] = '%'.$value.'%';

                if($field != $last_field) $context['conditions'] .= ' '.$search_glue.' ';

            }

            $queryBuilder->where($context['conditions'], $context['bind']);
        }

        // Get the Phalcon Query Language output to use as APC key
        $phql  = $queryBuilder->getPhql();

        // Check if records exist in cache
        if(apc_exists($phql)) {

            $records = apc_fetch($phql);


            // Otherwise run the query
        }else{

            $records = $queryBuilder->getQuery()->execute()->setHydrateMode(Resultset::TYPE_RESULT_FULL)->toArray();

            apc_store($phql, $records);

        }

        //return $count;

    }

    /**
     * Get list
     *
     * @var string
     */
    public function getList($context = array())
    {

        $router = new \Phalcon\Mvc\Router();
        $router->handle();
        $name =  ucfirst($router->getControllerName());

        $fields = $this->columns ?: $this->allowedFields['columns'];

        $context = array_merge(array(
            'models'    => array($name),
            'limit'     => $this->limit,
            'offset'    => $this->offset,
            'order'     => $this->sort .' '. $this->direction,
            'columns'   => $fields
        ), $context);

        $queryBuilder = new \Phalcon\Mvc\Model\Query\Builder($context);

        // Build search conditions
        if($this->isSearch){

            $context['conditions'] = '';
            $context['bind'] = array();

            $search_glue = $this->searchType == 'filter' ? 'AND' : 'OR';

            $last_field = @end(array_keys($this->searchFields));
            foreach ($this->searchFields as $field => $value) {

                $context['conditions'] .= " $field LIKE :{$field}_value:";
                $context['bind'][$field.'_value'] = '%'.$value.'%';

                if($field != $last_field) $context['conditions'] .= ' '.$search_glue.' ';

            }

            $queryBuilder->where($context['conditions'], $context['bind']);
        }

        // Get the Phalcon Query Language output to use as APC key
        $phql  = $queryBuilder->getPhql();

        // Check if records exist in cache
   //     if(apc_exists($phql)) {

    //        $records = apc_fetch($phql);


        // Otherwise run the query
   //     }else{

            $records = $queryBuilder->getQuery()->execute()->setHydrateMode(Resultset::TYPE_RESULT_FULL)->toArray();

     //       apc_store($phql, $records);

       // }

        return $this->provide($records);

    }


    public function addItem()
    {

        // Getting a request instance
        $request = new \Phalcon\Http\Request();

        $data = $request->getPost();

        $data['created_on'] = $data['modified_on'] = date('Y-m-d H:i:s'); // TODO: Format data based on BD type (make agnostic)
        $data['version']    = 1;

        $router = new \Phalcon\Mvc\Router();
        $router->handle();

        // TODO: Inflector here
        $name =  ucfirst($router->getControllerName());


        $name = 'Companies';

        $model = new $name();

        if($model->create($data) !== false){

           // var_dump($model->id);
            return $this->provide($this->getItem($model->id),
                                  \Momentum\Responses\Http::CREATED);

        }else{

            echo "Umh, We can't store that right now: \n";
            foreach ($model->getMessages() as $message) {
                echo $message, "\n";
            }

        };

    }

    public function getItem($id)
    {
        $router = new \Phalcon\Mvc\Router();
        $router->handle();
        $name =  ucfirst($router->getControllerName());

        // TODO: Inflector here
        $name = 'Companies';

        $model = new $name();

        $records = $model::find($id);

        if(!count($records)) return false;

        $response = array();
        foreach($records as $record)
            $response[] = $record->toArray();

        return $response;

    }

    /**
     * Get a single property by ID
     *
     * @var string
     */
    public function fetchItem($id)
    {
        $response = $this->getItem($id);

        return $this->provide($response);
    }

    public function noId()
    {
        throw new \Momentum\Exceptions\HTTPException(
            'Missing function call parameters',
            403,
            array(
                'dev' => 'Cannot access property without ID.',
                'internalCode' => 'NF1000',
                'more' => 'Ensure URL follows the format /property/{id}'
            )
        );
    }
}