<?php

return call_user_func(function(){

    $companiesCollection = new \Phalcon\Mvc\Micro\Collection();

    $companiesCollection->setPrefix('/companies')
        ->setHandler('\Momentum\Controllers\CompaniesController')
        ->setLazy(true);

    // Set Access-Control-Allow headers.
//    $propertiesCollection->options('/', 'optionsBase');
//    $propertiesCollection->options('/{id}', 'optionsOne');

    // First paramter is the route, which with the collection prefix here would be GET /example/
    // Second paramter is the function name of the Controller.

    $companiesCollection->get('/{id:[0-9]+}', 'getItem');
    $companiesCollection->get('/', 'getList');
    $companiesCollection->post('/', 'getList');

    // This is exactly the same execution as GET, but the Response has no body.
    $companiesCollection->head('/{id:[0-9]+}', 'getItem');
    $companiesCollection->head('/', 'getList');


    $companiesCollection->post('/', 'addItem');

    // $id will be passed as a parameter to the Controller's specified function
//    $propertiesCollection->get('/{id:[0-9]+}', 'getOne');
//    $propertiesCollection->head('/{id:[0-9]+}', 'getOne');
//    $propertiesCollection->post('/', 'post');
//    $propertiesCollection->delete('/{id:[0-9]+}', 'delete');
//    $propertiesCollection->put('/{id:[0-9]+}', 'put');
//    $propertiesCollection->patch('/{id:[0-9]+}', 'patch');

    return $companiesCollection;

});