<?php
/**
 * By default, namespaces are assumed to be the same as the path.
 * This function allows us to assign namespaces to alternative folders.
 * It also puts the classes into the PSR-0 autoLoader.
 */
$loader = new \Phalcon\Loader();

$loader->registerNamespaces(array(
//	'Models' => __DIR__ . '/models/',
    'Momentum\Controllers' => __DIR__ . '/controllers/',
    'Momentum\Exceptions' => __DIR__ . '/exceptions/',
    'Momentum\Responses' => __DIR__ . '/responses/',
    'Momentum\Responses\Schemas' => __DIR__ . '/responses/schemas'
))->register();

$loader->registerDirs(array(__DIR__ .'/models/'))->register();

/**
 * The DI is our direct injector.  It will store pointers to all of our services
 * and we will insert it into all of our controllers.
 * @var DefaultDI
 */
$di = new \Phalcon\DI\FactoryDefault();

/**
 * Return array of the Collections, which define a group of routes, from
 * routes/collections.  These will be mounted into the app itself later.
 */
$di->set('collections', function() { return include('./routes/routeLoader.php'); });

/**
 * $di's setShared method provides a singleton instance.
 * If the second parameter is a function, then the service is lazy-loaded
 * on its first instantiation.
 */
$di->setShared('config', function() { return new IniConfig("config/config.ini"); });

// As soon as we request the session service, it will be started.
$di->setShared('session', function() {
    $session = new \Phalcon\Session\Adapter\Files();
    $session->start();
    return $session;
});

/**
 * Database setup.
 */
$di->set('db', function() {
    return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
        "host" => "localhost",
        "username" => "root",
        "password" => "",
        "dbname" => "restkit-test",
        'charset' => 'utf8',
        "options" => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ));
});






